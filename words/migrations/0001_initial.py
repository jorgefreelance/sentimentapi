# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Word',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('french', models.CharField(max_length=50, unique=True, null=True, verbose_name=b'Word in french', blank=True)),
                ('italian', models.CharField(max_length=50, unique=True, null=True, verbose_name=b'Word in italian', blank=True)),
                ('score', models.IntegerField(default=0, verbose_name=b'Word score', validators=[django.core.validators.MaxValueValidator(2, b'Score value greater than 2'), django.core.validators.MinValueValidator(-2, b'Score value less than -2')])),
            ],
        ),
    ]
