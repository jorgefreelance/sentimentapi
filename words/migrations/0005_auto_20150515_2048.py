# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
import words.models


class Migration(migrations.Migration):

    dependencies = [
        ('words', '0004_auto_20150508_0708'),
    ]

    operations = [
        migrations.AlterField(
            model_name='frenchword',
            name='score',
            field=models.IntegerField(verbose_name=b'Score', validators=[django.core.validators.MaxValueValidator(2, b'Score value greater than 2'), django.core.validators.MinValueValidator(-2, b'Score value less than -2'), words.models.NotEqualTo0]),
        ),
        migrations.AlterField(
            model_name='italianword',
            name='score',
            field=models.IntegerField(verbose_name=b'Score', validators=[django.core.validators.MaxValueValidator(2, b'Score value greater than 2'), django.core.validators.MinValueValidator(-2, b'Score value less than -2'), words.models.NotEqualTo0]),
        ),
    ]
