# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('words', '0003_auto_20150418_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='FrenchWord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('word', models.CharField(unique=True, max_length=50, verbose_name=b'French word')),
                ('score', models.IntegerField(default=0, verbose_name=b'Score', validators=[django.core.validators.MaxValueValidator(2, b'Score value greater than 2'), django.core.validators.MinValueValidator(-2, b'Score value less than -2')])),
            ],
        ),
        migrations.CreateModel(
            name='ItalianWord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('word', models.CharField(unique=True, max_length=50, verbose_name=b'Italian word')),
                ('score', models.IntegerField(default=0, verbose_name=b'Score', validators=[django.core.validators.MaxValueValidator(2, b'Score value greater than 2'), django.core.validators.MinValueValidator(-2, b'Score value less than -2')])),
            ],
        ),
        migrations.DeleteModel(
            name='StopWord',
        ),
        migrations.DeleteModel(
            name='Word',
        ),
    ]
