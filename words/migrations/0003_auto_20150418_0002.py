# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('words', '0002_stopwords'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='StopWords',
            new_name='StopWord',
        ),
    ]
