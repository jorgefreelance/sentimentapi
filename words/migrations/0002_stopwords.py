# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('words', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='StopWords',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('french', models.CharField(max_length=50, unique=True, null=True, verbose_name=b'Stop word in french', blank=True)),
                ('italian', models.CharField(max_length=50, unique=True, null=True, verbose_name=b'Stop word in italian', blank=True)),
            ],
        ),
    ]
