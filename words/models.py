# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError


def NotEqualTo0(value):
    if value == 0:
        raise ValidationError('Word cannot be scored with 0 '
                              '(to avoid exceptions when exporting'
                              ' and importing the list into the database)')


class FrenchWord(models.Model):

    def __str__(self):
        return u"word: {} - score: {}".format(self.word, self.score)

    word = models.CharField(
        verbose_name="French word", unique=True, max_length=50)
    score = models.IntegerField(verbose_name="Score",
                                validators=[MaxValueValidator(2, "Score value greater than 2"),
                                            MinValueValidator(-2,
                                                              "Score value less than -2"),
                                            NotEqualTo0])


class ItalianWord(models.Model):

    def __str__(self):
        return u"word: {} - score: {}".format(self.word, self.score)

    word = models.CharField(
        verbose_name="Italian word", unique=True, max_length=50)
    score = models.IntegerField(verbose_name="Score",
                                validators=[MaxValueValidator(2, "Score value greater than 2"),
                                            MinValueValidator(-2,
                                                              "Score value less than -2"),
                                            NotEqualTo0])
