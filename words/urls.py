from words.views import APIRoot, FRAnalysis, ITAnalysis
from django.conf.urls import url
from rest_framework.authtoken import views

urlpatterns = [
    url(r'fr/', FRAnalysis.as_view(), name="french"),
    url(r'ita/', ITAnalysis.as_view(), name="italian"),
    url(r'^$', APIRoot.as_view(), name="api-root"),
    url(r'token-auth/', views.obtain_auth_token, name="token-auth"),
]
