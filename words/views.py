# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework import views
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework import status
import re
import json

from .models import FrenchWord
from .models import ItalianWord


def score(text, lang="fr"):
    """ From the database get the score for each word

    and return the global score
    """
    words = re.finditer(r"[\w']+|[.,!?;]", text.lower())
    globalscore = 0
    for word in words:
        q = None
        try:
            if lang == "fr":
                q = FrenchWord.objects.get(word=word.group(0))
            elif lang == "ita":
                q = ItalianWord.objects.get(word=word.group(0))
        except ObjectDoesNotExist:
            pass
        finally:
            if q is not None:
                globalscore = globalscore + q.score
    return globalscore


class APIRoot(views.APIView):

    """ Here you can find the available endpoints for your application to use
    """

    def get(self, request):
        return Response({
            'fr': reverse("french", request=request),
            'ita': reverse("italian", request=request)
        })


class FRAnalysis(views.APIView):

    u""" ## French text sentiment analysis

    The score goes from -2 to +2:

    * -2: very negative
    * -1: negative
    * 0: neutral
    * 1: positive
    * 2: very positive

    The API expects you to send a field called `text` with the text
    in French you want to score. Then, the API will return a response
    with the field `score` stating the global score of the input.

    ### example

    Use the form below to send data to the API end-point. Leave *Media type*
    as *application/json*.

        {"text": "GNU/Linux est un excellent système d'exploitation."}
    """
    authentication_classes = []
    permission_classes = []

    def get(self, request):
        return Response()

    def post(self, request, *args, **kwargs):
        """ Gets a input text and return the score of such text
        """
        text = request.DATA.get("text", "")
        if not text:
            return Response(status.HTTP_400_BAD_REQUEST)
        result = score(text)
        if result == 0:
            r = "neutral"
        elif result == 1:
            r = "positive"
        elif result >= 2:
            r = "very positive"
        elif result == -1:
            r = "negative"
        elif result <= -2:
            r = "very negative"
        return Response({"score": r})


class ITAnalysis(views.APIView):

    u""" ## Italian text sentiment analysis

    The score goes from -2 to +2:

    * -2: very negative
    * -1: negative
    * 0: neutral
    * 1: positive
    * 2: very positive

    The API expects you to send a field called `text` with the text
    in Italian you want to score. Then, the API will return a response
    with the field `score` stating the global score of the input.

    ### example

    Use the form below to send data to the API end-point. Leave *Media type*
    as *application/json*.

         {"text": "Questa pizza è proprio buona!"}
    """
    authentication_classes = []
    permission_classes = []

    def get(self, request):
        return Response()

    def post(self, request, *args, **kwargs):
        """ Gets a input text and return the score of such text
        """
        text = request.DATA.get("text", "")
        if not text:
            return Response(status.HTTP_400_BAD_REQUEST)
        result = score(text, "ita")
        if result == 0:
            r = "neutral"
        elif result == 1:
            r = "positive"
        elif result >= 2:
            r = "very positive"
        elif result == -1:
            r = "negative"
        elif result <= -2:
            r = "very negative"
        return Response({"score": r})
