from django.forms import widgets
from rest_framework import serializers

class InputSerializer(serializers.Serializer):
    text = serializers.CharField(label="Text to analyze", style={'base_template': 'textarea.html'})
