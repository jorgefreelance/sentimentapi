from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportMixin

from .models import FrenchWord
from .models import ItalianWord

# import_export resources


class FrenchWordResource(resources.ModelResource):

    class Meta:
        model = FrenchWord
        import_id_fields = ('word',)


class ItalianWordResource(resources.ModelResource):

    class Meta:
        model = ItalianWord
        import_id_fields = ('word',)

# Integration with admin panel


class FrenchWordAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = FrenchWordResource
    pass


class ItalianWordAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = ItalianWordResource
    pass

admin.site.register(FrenchWord, FrenchWordAdmin)
admin.site.register(ItalianWord, ItalianWordAdmin)
