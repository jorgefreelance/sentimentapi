from django.apps import AppConfig

class WordConfig(AppConfig):
    name = 'words'
    verbose_name = "List of words for French and Italian"
