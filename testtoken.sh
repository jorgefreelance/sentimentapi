#!/usr/bin/sh

# This file is use for testing the token authentication against the API
curl -X POST http://127.0.0.1:8000/api/fr/ -H "Authorization: Token $1" -H "Content-Type: application/json" -d '{"text":"this is a test"}'
