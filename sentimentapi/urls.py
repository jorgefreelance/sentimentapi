from django.conf.urls import include, url
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.shortcuts import render


def index(request):
    """ Display an Index page
    """
    adminurl = reverse("admin:index")
    apiurl = reverse("api-root")
    result = [{"name": "Administration panel", "url": adminurl},
              {"name": "API index", "url": apiurl}]
    context = {"urls": result}
    return render(request, "index.html", context)

urlpatterns = [
    # Examples:
    # url(r'^$', 'sentimentapi.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', index, name="index"),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include('words.urls')),
]
