import logging
import traceback
import pprint
from StringIO import StringIO

logger = logging.getLogger('heroku')


def unhandledexception(excType, excValue, tracebackobj):
    """
    @param excType exception type
    @param excValue exception value
    @param tracebackobj traceback object
    """
    tracebackinfo = StringIO()
    # Print the traceback
    traceback.print_tb(tracebackobj, None, tracebackinfo)
    # Print all the variables when the exception happened
    tracebackinfo.write(
        "\nAll defined variables when the exception happened:\n")
    pprint.pprint(tracebackobj.tb_frame.f_globals, tracebackinfo)
    tracebackinfo.seek(0)

    # Send the gathered information into the logs
    logger.critical(tracebackinfo.read())
